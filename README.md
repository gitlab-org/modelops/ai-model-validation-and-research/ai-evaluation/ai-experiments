# AI Chat Experiments and Data for Diagnostic Testing 

## Overview 

This is the developer's guide on how to choose the right subset of data from the Centralised Evaluation Framework for Diagnostic Testing of Duo Chat. Leveraging a subset of the data focused around a specific objective or use case enables rapid iteration and experimentation with prompt changes.

## Topics to Cover 

1. The Datasets Currently Available
2. The Subset Dataset and Why Testing Only with a Subset Dataset is Recommended for Diagnostic Testing
3. Responsible Execution of Pipelines: Guidelines for Running These Pipelines
4. Storage Locations for Your Output and Version Control
5. Weekly Changes to the Subset Dataset
6. Weekly and Daily Updates on the Centralized Evaluation Framework


## The Datasets Currently Available


The datasets currently available consist of an Issue/Epic-related dataset with approximately 1600 rows of data and MBPP non-GitLab specific code generation data. Both datasets are accessible in BigQuery as individual tables and also as a combined table. 

Big query Table Names based on task_type 

1. [Task: Issue/Epic, Source: GitLab](https://console.cloud.google.com/bigquery?authuser=0&project=dev-ai-research-0e2f8974&ws=!1m5!1m4!4m3!1sdev-ai-research-0e2f8974!2sduo_chat_experiments!3s2023-12-08_make_dataset_2_tmp)
2. [Task: Code Generation, Source: MBPP: Link](https://console.cloud.google.com/bigquery?q=search&referrer=search&page=table&p=dev-ai-research-0e2f8974&d=code_generation&t=mbpp_all_sanitized&project=dev-ai-research-0e2f8974&authuser=0&ws=!1m5!1m4!4m3!1sdev-ai-research-0e2f8974!2scode_generation!3smbpp_all_sanitized)
3. [Combined Table with Metrics](https://console.cloud.google.com/bigquery?hl=en&project=dev-ai-research-0e2f8974&ws=!1m5!1m4!4m3!1sdev-ai-research-0e2f8974!2sduo_chat_experiments!3smon_results_combined)
4. [The datasets store in dataset folder for Diagnostic Testing](https://gitlab.com/gitlab-org/modelops/ai-model-validation-and-research/ai-evaluation/datasets)

Below is a rough schema of the combined  table.

| field name |
| ------ |
|  data_source      | 
|  task_type      |   
| question      |  
| answering_model       |  
| answer       |  
| evaluating_model       |  
| correctness       |  
| readability       |  
| comprehensiveness       |  
| explanation       |  
| llm_error       |  
| comparison_model       |  
| answer_from_comparison_model       |  
| comparison_similarity       |  

## The Subset Dataset and Why Testing Only with a Subset Dataset is Recommended for Diagnostic Testing

The Diagnostic Test is intended to be a rapid, low-cost experiment for developers to have confidence in the changes they make to tools and prompts as they iterate on code. Diagnostic Tests are not meant to be understanding how chat is working at scale for every code change. Instead, the Centralised Evaluation Framework serves that purpose with the daily runs. 

Every week based on the data and metric  performance the Model Validation team can assist in creating a subset dataset that could be stored as a LFS, JSON, or XML as an output from the dashboard. This subset of data will be the basis for running an experiment with a well defined objective . This dataset would be based on areas where chat is not performing well, allowing developers to focus and iterate on areas where Chat Duo is weakest. 

 
As an example, we examine the dashboard for this week with the following goal/experiment in mind:

1. Objective: To make chat as good as the foundational API claude-2 model 
2. Metric: Comparison Similarity of Answering model (Duo Chat) and Comparison model (Claude) 
3. Dataset: Subset of Centralised Evaluation Framework in the Dataset folder for similarity score range 1 to 71.  
4. Number of distinct questions: 81
5. Control Metric Score: 0.57
5. Details of Experiment:
7. Experiment Metric Score: 
8. Control vs Experiment Variance:
9. Experiment Success: Yes /No  pushed to Main and measured if it holds at scale with Centralised evaluation framework run by end of the week

The data can be dissected through the [dashboard](https://lookerstudio.google.com/u/0/reporting/151b233a-d6ad-413a-9ebf-ea6efbf5387b/page/p_953s8iaied).  Here we have 81 rows of data that represent a mix of issue/epic and code generation use cases with poor performance, out of which 5 are code generation prompts.. Focusing on these prompts across both use cases/tasks allows us to hone in on weaker areas, while still incorporating diversity in the test data set which allows it to remain a good proxy of the full data set. Keeping diversity within the test data is crucial, as crafting prompt templates for one task may have an impact on the other.


## Responsible Execution of Pipelines: Guidelines for Running These Pipelines

We advise against developers running  the pipelines on the full dataset, as the full data set is already being run by the Centralised Evaluation Framework. Duplication in this space leads to higher costs and quota errors. We strongly encourage developers to run their diagnostic testing on a subset of data only.



## Weekly Changes to the Subset Dataset

As developers make progress with prompt changes , we anticipate that the subset of data that developers are using for diagnostic testing will change. As validation scores improve in weaker areas, new areas of focus within the datasets will emerge. 

Hence we anticipate updating the subset of data, delivered as an LFS file, for developer experimentation on a weekly basis. Depending on gains in similarity scores, there may be some weeks where the subset of data is unchanged.


## Weekly and Daily Updates on the Centralized Evaluation Framework

We plan to run the Centralized Evaluation Framework daily starting from next week , pending completion of our automation pipeline. It is through the daily run and update in the dashboard we would quickly be able to understand how the changes have impacted the full dataset at scale and plan to add recommendations as we go in this issue.


## Current State of Diagnostic Testing 

**Currently we can iterate and experiment with prompt templates for the code generation data, working towards a change in that metric.** This experimentation can occur while the AI Framework team works on the Rake Task to enable preseeding of the Issue/Epic Context Data. We should start with rapid prompt experimentation for the code generation task (5 prompts), followed by diagnostic testing against the entire subset of data (81 prompts). This can be concurrent with iteration with the rake task, as there are areas of improvement in chat that can mutually benefit both the tasks.  







